# legislation-ai-summary-tool

## Overview
This policy tech tool will provide concise bill summaries.

## Tech Stack (under development)
- Back end: Python, Django?
- Front end: Typescript, Vite?
- APIs: OpenAI, LegiScan

## MVP:
The app should:
- Use the LegiScan API to get full bill text (California)
- Output bill summaries in 300 words using OpenAI.